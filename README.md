# Golang UUID compare

##### Packages
* [github.com/satori/go.uuid](https://github.com/satori/go.uuid)
* [github.com/google/uuid](https://github.com/google/uuid)

##### UUID versions
* Satori: 1, 2, 3, 4, 5
```golang
// version = 1
func NewV1() (UUID, error) {
	// ...
}

// version = 2
func NewV2(domain byte) (UUID, error) {
	// ...
}

// version = 3
func NewV3(ns UUID, name string) UUID {
	// ...
}

// version = 4
func NewV4() (UUID, error) {
	// ...
}

// version = 5
func NewV5(ns UUID, name string) UUID {
	// ...
}
```
* Google: 1, 4
```golang
// version = 1
func NewUUID() (UUID, error) {
    // ...
}

// version = 4
func New() UUID {
	// ...
}
```

##### Testing
```golang
geUuid := google.New()
siUuid, err := satori.NewV4()

print(geUuid.String())
print(siUuid.String())
```

```bash
github.com/google/uuid          7726e37a-135e-4984-9860-554ea2508d27
github.com/satori/go.uuid       017584c7-3a00-4ee9-bf32-7e46ffd5b585
```

##### Benchmark
```bash
go test -v -bench=. -benchmem

BenchmarkGoogleUUID-4            2000000               896 ns/op              64 B/op          2 allocs/op
BenchmarkSatoriUUID-4            2000000               876 ns/op              64 B/op          2 allocs/op
BenchmarkCustomUUID-4            2000000               865 ns/op              64 B/op          2 allocs/op
```

### Dive compare
##### Create by Satori & Google
* [source by github.com/satori/go.uuid](https://github.com/satori/go.uuid/blob/36e9d2ebbde5e3f13ab2e25625fd453271d6522e/generator.go#L166)
```golang
func (g *rfc4122Generator) NewV4() (UUID, error) {
    u := UUID{}
    if _, err := g.rand.Read(u[:]); err != nil {
        return Nil, err
    }
    u.SetVersion(V4)
    u.SetVariant(VariantRFC4122)

    return u, nil
}
```

* [source by github.com/google/uuid](https://github.com/google/uuid/blob/dec09d789f3dba190787f8b4454c7d3c936fed9e/version4.go#L29)
```golang
func NewRandom() (UUID, error) {
    var uuid UUID
    _, err := io.ReadFull(rander, uuid[:])
    if err != nil {
        return Nil, err
    }
    uuid[6] = (uuid[6] & 0x0f) | 0x40 // Version 4
    uuid[8] = (uuid[8] & 0x3f) | 0x80 // Variant is 10
    return uuid, nil
}
```

##### Used randomizer
* Satori
```golang
    import "crypto/rand"
    // ...

    var global = newRFC4122Generator()

    func newRFC4122Generator() Generator {
    	return &rfc4122Generator{
            // ...
    		rand:       rand.Reader,
    	}
    }

    // ...

    // (g *rfc4122Generator)

    if _, err := g.rand.Read(u[:]); err != nil {
        return Nil, err
    }
```
* Google
```golang
    import "crypto/rand"
    // ...

    var rander = rand.Reader

    // ...

    // func ReadFull(r Reader, buf []byte) (n int, err error)
    // ...
    // nn, err = r.Read(buf[n:])
    // ...

    _, err := io.ReadFull(rander, uuid[:])

```
* one look like two, with use [crypto/rand](https://github.com/golang/go/tree/master/src/crypto/rand)

##### Version & variant
* Satori
```golang
    const V4 = 4

    // ...

    func (u *UUID) SetVersion(v byte) {
    	u[6] = (u[6] & 0x0f) | (v << 4)
    }

    func (u *UUID) SetVariant(v byte) {
    	switch v {
    	// ...
    	case VariantRFC4122:
    		u[8] = (u[8]&(0xff>>2) | (0x02 << 6))
        // ...
    	}
    }

	u.SetVersion(V4)
	u.SetVariant(VariantRFC4122)
```
* Google
```golang
	uuid[6] = (uuid[6] & 0x0f) | 0x40 // Version 4
	uuid[8] = (uuid[8] & 0x3f) | 0x80 // Variant is 10
```
* Version & variant same
