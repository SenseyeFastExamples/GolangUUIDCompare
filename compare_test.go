package main

import (
	google "github.com/google/uuid"
	satori "github.com/satori/go.uuid"
	"testing"
)

func BenchmarkGoogleUUID(b *testing.B) {
	for i := 0; i < b.N; i++ {
		uuid := google.New()

		uuid.String()
	}
}

func BenchmarkSatoriUUID(b *testing.B) {
	for i := 0; i < b.N; i++ {
		uuid, err := satori.NewV4()

		must(err)

		_ = uuid.String()
	}
}

func BenchmarkCustomUUID(b *testing.B) {
	for i := 0; i < b.N; i++ {
		uuid, err := uuidV4()

		must(err)

		_ = uuid
	}
}
