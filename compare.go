package main

import (
	"crypto/rand"
	"encoding/hex"
	"fmt"
	google "github.com/google/uuid"
	satori "github.com/satori/go.uuid"
)

func main() {
	geUuid := google.New()
	siUuid, err := satori.NewV4()

	if err != nil {
		fmt.Printf("error on generate by github.com/satori/go.uuid\n")
	}

	uuid, err := uuidV4()

	if err != nil {
		fmt.Printf("error on generate by custom\n")
	}

	fmt.Printf("github.com/google/uuid      %s\n", siUuid.String())
	fmt.Printf("github.com/satori/go.uuid   %s\n", geUuid.String())
	fmt.Printf("custom uuidV4               %s\n", uuid)
}

func uuidV4() (string, error) {
	uuid := [16]byte{}

	if _, err := rand.Reader.Read(uuid[:]); err != nil {
		return "", err
	}

	uuid[6] = (uuid[6] & 0x0f) | 0x40
	uuid[8] = (uuid[8] & 0x3f) | 0x80

	var buf [36]byte

	hex.Encode(buf[0:8], uuid[0:4])
	buf[8] = '-'
	hex.Encode(buf[9:13], uuid[4:6])
	buf[13] = '-'
	hex.Encode(buf[14:18], uuid[6:8])
	buf[18] = '-'
	hex.Encode(buf[19:23], uuid[8:10])
	buf[23] = '-'
	hex.Encode(buf[24:], uuid[10:])

	return string(buf[:]), nil
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}
